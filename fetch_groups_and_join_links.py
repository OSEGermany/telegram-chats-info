#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2022 Robin Vobruba <hoijui.quaero@gmail.com>
# SPDX-License-Identifier: Unlicense

'''
Gets all group names and join-links from the users dialogs,
and connects them if possible.
For info on how to use this file,
see _README.md_.
'''

from __future__ import print_function

import asyncio
import csv
import os
import re
import time
import sys
import yaml
from telethon import TelegramClient

GROUP_NAME_RGX_OSEG = r"^(\[ OSEG(eV(\+)?)? \] |OpenEcoLab \| |\[ OpenEcoLab \] )"
MISSING_JOIN_LINK = 'MISSING'

def eprint(*args, **kwargs):
    """ Like 'print', but writes to stderr instead. """
    print(*args, file=sys.stderr, **kwargs)

def is_join_link_valid(_join_link):
    """ Checks if a join link has a valid format (local only). """
    #join_link_hash = re.findall(r"[a-zA-Z0-9]+$", join_link)
    #return len(join_link_hash[0]) == 22
    # private join-links use hashes only, but public ones might be any kind of string
    return True

def get_group_name(join_link):
    """
    Returns the name/title of the Telegram group linked to by the given join-link.
    It does so by fetching and parsing the HTML response
    of the HTTP(S) GET request of the join-link itsself.
    """
    stream = os.popen(
        ("curl -q {} 2> /dev/null | grep -e 'property=\\\"og:title'"
         + " | sed -e 's|.* content=\\\"||' -e 's|\\\">$||'")
        .format(join_link))
    output = stream.read().strip()
    return output

async def find_join_links(client, entity_name):
    """
    Extracts join-links out of all chat messages from the given entity (eg. dialog).
    """
    join_links = []
    async for message in client.iter_messages(entity_name, search='https://t.me/joinchat/'):
        # private join-links use hashes only, but public ones might be any kind of string
        join_links = join_links + re.findall(r"https://t.me/joinchat/[^ \n\r]+",
                                             str(message.text))
    return join_links

def fitting_name(rgx, name):
    """
    Indicates whether the given name/title
    fits into the scheme of groups
    we are interested in,
    which is defined by a regex.
    """
    return re.search(rgx, name) is not None

def list_sort_uniq(my_list):
    """
    Sorts a list, and makes sure it has no double-entries.
    """
    my_list = list(dict.fromkeys(my_list))
    my_list.sort()
    return my_list

async def find_participating_groups_and_join_links_and_admins(client, group_name_rgx):
    """
    Returns fitting group names
    and to each a list of join-links found within the messages in its dialog.
    """
    group_name_2_join_links = {}
    group_name_2_admins = {}
    dialogs = await client.get_dialogs(limit=0)
    dialogs_total = dialogs.total
    dialogs_counter = 0
    start_time = time.time()
    async for dialog in client.iter_dialogs():
        dialogs_counter += 1
        if not fitting_name(group_name_rgx, dialog.title):
            print('Skipping non-fitting group "%s"!' % dialog.title)
            continue
        if dialog.archived:
            print('Skipping archived dialog!')
            continue
        if not dialog.title:
            print('Skipping empty title dialog!')
            continue
        # skip the dialog if it is not a group (== chat or channel)
        if not dialog.is_group:
            eprint('WARNING: Skipping fitting, but non-group dialog "%s"!' % dialog.title)
            continue
        elapsed_seconds = time.time() - start_time
        eta_time = start_time + int(float(elapsed_seconds) * dialogs_total / dialogs_counter)
        print("Working on dialog %d/%d (runtime: %ds | ETA: '%s' | '%s') ..."
              % (dialogs_counter, dialogs_total, elapsed_seconds,
                 time.ctime(eta_time), dialog.title))
        # extract all join links from all messages in this dialog
        # and add them to the list
        print("   searching join-links ...")
        join_links = await find_join_links(client, dialog.title)
        group_name_2_join_links[dialog.title] = join_links

        print("   iterating over admins ...")
        from telethon.tl.types import ChannelParticipantsAdmins
        admins = []
        async for user in client.iter_participants(dialog, filter=ChannelParticipantsAdmins):
            admins.append(user.first_name)
        admins.sort()
        group_name_2_admins[dialog.title] = admins

        print("   done.")
    return (group_name_2_join_links, group_name_2_admins)

def filter_join_links(group_name_2_join_links):
    """
    For each group: filters out the associated join-link
    that links to that given group, and stores them as a tuple.
    If no such link was found, a placeholder-string is used instead,
    and a warning message is printed.
    """
    group_name_2_join_link = {}
    for group in group_name_2_join_links:
        this_group_join_link = None
        join_links = group_name_2_join_links[group]
        # see if one of them is the correct link to this group
        for join_link in join_links:
            link_group_name = get_group_name(join_link)
            print('        join-link: {} -> group-name: "{}"'.format(join_link, link_group_name))
            if link_group_name == group:
                this_group_join_link = join_link
                break
        if not this_group_join_link:
            this_group_join_link = MISSING_JOIN_LINK
            eprint('WARNING: Group chat contains no join-link to its-self: "%s"!' % group)
        group_name_2_join_link[group] = this_group_join_link
    return group_name_2_join_link

def write_list_to_file(file_path, lst):
    """
    Writes the contents of a list to a file,
    using new-line as a separator.
    """
    out_file = open(file_path, 'a')
    for list_item in lst:
        out_file.write('{}\n'.format(list_item))
    out_file.close()

async def fetch_parse_filter_write():
    """
    Fetches online dialog info,
    parses and filters it,
    and stores it into files.
    """
    output_dir = 'output'
    group2link_csv_file = os.path.join(output_dir, 'groups2join_link.csv')

    if not os.path.exists(output_dir):
        os.mkdir(output_dir)

    with open("settings.yml", 'r') as settings_stream:
        try:
            settings = yaml.safe_load(settings_stream)
        except yaml.YAMLError as exc:
            print(exc)

    client = await TelegramClient('osegCannelsInfo', settings['api_id'],
                                  settings['api_hash']).start()

    authorized = await client.is_user_authorized()
    if not authorized:
        client.send_code_request(settings['phone_number'])
        _ = client.sign_in(settings['phone_number'], input('Enter code: '))

    group_name_rgx = GROUP_NAME_RGX_OSEG
    if len(sys.argv) > 1:
        group_name_rgx = sys.argv[1]

    print('\n\n## Fetching group names and join links from all your dialogs ...')
    print('\n\n### fitting regex "{}" ...'.format(group_name_rgx))
    (group_name_2_join_links, group_name_2_admins) = await find_participating_groups_and_join_links_and_admins(client, group_name_rgx)

    print('\n\n## Filter join-links ...')
    group_name_2_join_link = filter_join_links(group_name_2_join_links)

    print('\n\n## Writing filtered info to "%s" ...' % group2link_csv_file)
    with open(group2link_csv_file, mode='w') as g2l_csv_file:
        g2lw = csv.writer(g2l_csv_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        g2lw.writerow(['group-name', 'join-link', 'admins'])
        for group in group_name_2_join_link:
            join_link = group_name_2_join_link[group]
            admins = group_name_2_admins[group]
            g2lw.writerow([group, join_link, '; '.join(admins)])

    print('\n## done.')

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(fetch_parse_filter_write())
    loop.close()
