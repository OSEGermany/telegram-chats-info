<!--
SPDX-FileCopyrightText: 2020-2022 Robin Vobruba <hoijui.quaero@gmail.com>
SPDX-License-Identifier: CC0-1.0
-->

[![REUSE status](
    https://api.reuse.software/badge/gitlab.com/OSEGermany/telegram-chats-info)](
    https://api.reuse.software/info/gitlab.com/OSEGermany/telegram-chats-info)

# Telegram Info fetcher

Scripts that gets all group names and join-links from the users dialogs (open chats),
and connects them if possible.

# Setup/Installation

For Debian/Ubuntu based distros:

1. Install python 3 and its package installer:\
    `sudo apt-get install python3-pip`
2. Install Telethon (the Telegram API) and the Python YAML library:\
    `pip3 install telethon pyyaml`
3. [Obtain a Telegram API ID](https://core.telegram.org/api/obtaining_api_id)
    for your user/telephone number.
4. `cp settings.TEMPLATE.yml settings.yml`,\
    and fill the values obtained in step 3. into "settings.yml".

# Usage

1. Run this script, and wait till it finishes:\
    `./fetch_groups_and_join_links.py`
    - Input your phone number and verify with the send code
    - *NOTE* Running this script might take a few minutes!
    - You may also supply an alternative (not OSEG tailored)
        group-name regex as first parameter:\
        `./fetch_groups_and_join_links.py "^MyGroupNamePrefix"`
2. If all goes well, you should now have a CSV file in the "output/" folder.
3. Have a look at it, to see which groups are "MISSING" a join-link
    to themselves, and try to generate and post such a link in those groups.
4. Run the script from step 5. again, to fetch the newly posted links as well.
5. Run the `.csv2md.py` script, which will generate a Markdown file
    from the CSV data, which you might want to use in public.
