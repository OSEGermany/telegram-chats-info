#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2022 Robin Vobruba <hoijui.quaero@gmail.com>
# SPDX-License-Identifier: Unlicense

'''
Tests some of the python code in this repo.
'''

import unittest
import fetch_groups_and_join_links as code

class TestStringMethods(unittest.TestCase):

    def test_oseg_group_name_regex(self):
        rgx = code.GROUP_NAME_RGX_OSEG
        self.assertTrue(code.fitting_name(rgx, '[ OSEG ] bla bla'))
        self.assertTrue(code.fitting_name(rgx, '[ OSEG+ ] bla bla'))
        self.assertTrue(code.fitting_name(rgx, '[ OSEGeV ] bla bla'))
        self.assertTrue(code.fitting_name(rgx, '[ OpenEcoLab ] bla bla'))
        self.assertTrue(code.fitting_name(rgx, 'OpenEcoLab | bla bla'))
        self.assertFalse(code.fitting_name(rgx, 'bla bla'))
        self.assertFalse(code.fitting_name(rgx, '[ OSEGev ] bla bla'))
        self.assertFalse(code.fitting_name(rgx, '[ OSEGx ] bla bla'))
        self.assertFalse(code.fitting_name(rgx, '[ OSEG- ] bla bla'))
        self.assertFalse(code.fitting_name(rgx, '[ OSEG* ] bla bla'))
        self.assertFalse(code.fitting_name(rgx, '[OSEG] bla bla'))
        self.assertFalse(code.fitting_name(rgx, '[ OSEG] bla bla'))
        self.assertFalse(code.fitting_name(rgx, '[OSEG ] bla bla'))
        self.assertFalse(code.fitting_name(rgx, '[OSEG+] bla bla'))
        self.assertFalse(code.fitting_name(rgx, '[ OSEG+] bla bla'))
        self.assertFalse(code.fitting_name(rgx, '[OSEG+ ] bla bla'))
        self.assertFalse(code.fitting_name(rgx, '[OSEGeV] bla bla'))
        self.assertFalse(code.fitting_name(rgx, '[ OSEGeV] bla bla'))
        self.assertFalse(code.fitting_name(rgx, '[OSEGeV ] bla bla'))
        self.assertFalse(code.fitting_name(rgx, '[OpenEcoLab] bla bla'))
        self.assertFalse(code.fitting_name(rgx, '[ OpenEcoLab] bla bla'))
        self.assertFalse(code.fitting_name(rgx, '[OpenEcoLab ] bla bla'))
        self.assertFalse(code.fitting_name(rgx, 'OpenEcoLab|bla bla'))
        self.assertFalse(code.fitting_name(rgx, 'OpenEcoLab |bla bla'))
        self.assertFalse(code.fitting_name(rgx, 'OpenEcoLab| bla bla'))

if __name__ == '__main__':
    unittest.main()

