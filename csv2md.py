#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2022 Robin Vobruba <hoijui.quaero@gmail.com>
# SPDX-License-Identifier: Unlicense

'''
Reads and parses a CSV file containing "name,url" rows
and writes it into an MD file as numbered links.
'''

from __future__ import print_function

import csv
import os
import sys

MISSING_JOIN_LINK = 'MISSING'

def eprint(*args, **kwargs):
    """ Like 'print', but writes to stderr instead. """
    print(*args, file=sys.stderr, **kwargs)

def csv2md():
    """
    Reads and parses a CSV file containing "name,url" rows
    and writes it into an MD file as numbered links.
    """
    output_dir = 'output'
    group2link_csv_file = os.path.join(output_dir, 'groups2join_link.csv')
    group2link_md_file = os.path.join(output_dir, 'groups2join_link.md')

    print('\n\n## Reading CSV data and write to MD ...')
    with open(group2link_csv_file, mode='r') as csv_file:
        csv_reader = csv.reader(csv_file)
        with open(group2link_md_file, mode='w') as md_file:
            md_file.write('# Gruppen-Einladungslinks\n\n')
            num = -1
            num_miss = 0
            for row in csv_reader:
                num += 1
                if num == 0:
                    # Skip the header line
                    continue
                if row[1] == MISSING_JOIN_LINK:
                    num_miss += 1
                    md_file.write('1. {}\n\n'.format(row[0]))
                else:
                    md_file.write(
                        '1. #Gruppeneinladungslink:\\\n   {}\\\n   [`{}`]({})\n\n'
                        .format(row[0], row[1], row[1]))
            md_file.write('\n')
            md_file.write('| | |\n')
            md_file.write('------- | ---\n')
            md_file.write('present | {}\n'.format(num - num_miss))
            md_file.write('missing | {}\n'.format(num_miss))
            md_file.write('**total** | **{}**\n'.format(num))

    print('\n## done.')

if __name__ == '__main__':
    csv2md()
